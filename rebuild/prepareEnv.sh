if [ -z ${CHROMIUM_TARGET_FOLDER+x} ];
then
    CHROMIUM_TARGET_FOLDER=..
fi

TOOLS_PATH=$(cd ./tools; pwd)

if [ -z ${CHROMIUMTYPO_PATH_APPLIED+x} ];
then
    export PATH=$TOOLS_PATH:$TOOLS_PATH/depot_tools:$TOOLS_PATH/depot_tools/ENV/bin:$PATH
    export CHROMIUMTYPO_PATH_APPLIED=1
fi

export CHROMIUMTYPO_ROOT=$(cd ./chromium; pwd)

function chromiumTypo_setArch {
   mkdir --parents $CHROMIUMTYPO_ROOT/src/out
   echo $1 > $CHROMIUMTYPO_ROOT/src/out/chromiumtypo_arch
}

function chromiumTypo_getArch {
   echo $(head -1 $CHROMIUMTYPO_ROOT/src/out/chromiumtypo_arch)
}

# К сожалению приходиться использовать этот хак для билдинга статической библиотеки, т.к. gyp не способен сгенерировать такую задачу
function chromiumTypo_ninjaReplaceSharedLibraryByStaticLibrary {
   local LIBTYPOCHROMIUM_NINJA=$CHROMIUMTYPO_ROOT/src/out/Release/chromiumtypo/libchromiumtypo.ninja
   sed -i '1i'`
          `'rule buildStaticLibrary\n'`
          `'  command = bash buildStaticLibrary.sh $out $in\n'`
          `'  description = buildStaticLibrary $out\n' \
          $LIBTYPOCHROMIUM_NINJA
   sed -i 's@build lib/libchromiumtypo.so lib/libchromiumtypo.so.TOC: solink@build lib/libchromiumtypo.a: buildStaticLibrary@g' $LIBTYPOCHROMIUM_NINJA
   sed -i 's@lib/libchromiumtypo.so@lib/libchromiumtypo.a@g' $CHROMIUMTYPO_ROOT/src/out/Release/build.ninja
}

