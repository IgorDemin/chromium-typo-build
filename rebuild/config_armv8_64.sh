#!/bin/bash

set -e

. ./prepareEnv.sh
export GYP_CROSSCOMPILE='1'
export GYP_DEFINES='OS=android target_arch=arm64 arm_version=8'
python $CHROMIUMTYPO_ROOT/chromiumtypo/gyp_chromiumtypo.py "$@"
chromiumTypo_ninjaReplaceSharedLibraryByStaticLibrary
chromiumTypo_setArch 'arm64-v8a'

