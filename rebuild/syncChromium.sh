#!/bin/bash

set -e

. ./prepareEnv.sh
pushd $CHROMIUMTYPO_ROOT
gclient sync --with_branch_heads --jobs 8
popd
