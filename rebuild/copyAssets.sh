#!/bin/bash

set -e

. ./prepareEnv.sh

rm -rf $CHROMIUM_TARGET_FOLDER/assets/

mkdir --parents $CHROMIUM_TARGET_FOLDER/assets/icu
mkdir --parents $CHROMIUM_TARGET_FOLDER/assets/webkit/res

cp $CHROMIUMTYPO_ROOT/src/third_party/icu/android/icudtl.dat $CHROMIUM_TARGET_FOLDER/assets/icu
cp $CHROMIUMTYPO_ROOT/src/third_party/WebKit/Source/core/css/*.css $CHROMIUM_TARGET_FOLDER/assets/webkit/res/
