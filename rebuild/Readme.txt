	How to rebuild

I. Prepare
    Get Ubuntu (for example in VirtualBox VM). Tested on Ubuntu 14.04.2 LTS
    git clone --recursive git@bitbucket.org:IgorDemin/chromium-typo-build.git
    go to tools folder
    bash syncChromium.sh

II. Build all:
    bash buildAll.sh

III. Partial build:
    bash config_armv7_32.sh
    bash build.sh

    (if you want to place result files to another directory, define CHROMIUM_TARGET_FOLDER environment variable)

    How to upgrade chromium

- See last stable <current_version>, <branch_commit> at https://omahaproxy.appspot.com/, android-stable
- Change url in chromium\.gclient
- Call: bash syncChromium.sh
- See <webkit_revision> in DEPS file
- Open repo chromium/src/third_party/WebKit
- Update original branch:
  git checkout original
  rm -rf !(.git|.|..)
  git clone https://chromium.googlesource.com/chromium/blink.git
  cd blink
  git checkout <webkit_revision>
  rm -rf .git LayoutTests ManualTests PerformanceTests
  mv * .[^.]* .. && rm -rf ../blink           (move all to parent)
  cd ..
  git add -A
  git commit -m "<current_version>"
- Merge "original" to "master"
- Test and apply fixes in "master" branch
- Change  custom_deps/Webkit in chromium\.gclient (points to commit in master branch)
- Rebuild all (see above)
- Make commit in chromium typo
- Push all changed
