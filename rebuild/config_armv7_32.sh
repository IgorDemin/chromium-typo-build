#!/bin/bash

set -e

. ./prepareEnv.sh
export GYP_CROSSCOMPILE='1'
export GYP_DEFINES='OS=android target_arch=arm arm_version=7'
python $CHROMIUMTYPO_ROOT/chromiumtypo/gyp_chromiumtypo.py "$@"
chromiumTypo_ninjaReplaceSharedLibraryByStaticLibrary
chromiumTypo_setArch 'armeabi-v7a'

