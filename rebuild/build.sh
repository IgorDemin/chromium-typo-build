#!/bin/bash

set -e

. ./prepareEnv.sh

ninja -C ${CHROMIUMTYPO_ROOT}/src/out/Release libchromiumtypo

ARCH=$(chromiumTypo_getArch)

mkdir --parents $CHROMIUM_TARGET_FOLDER/lib/$ARCH/
cp ${CHROMIUMTYPO_ROOT}/src/out/Release/lib/libchromiumtypo.a $CHROMIUM_TARGET_FOLDER/lib/$ARCH/

