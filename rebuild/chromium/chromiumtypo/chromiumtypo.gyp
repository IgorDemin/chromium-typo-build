{
  'targets': [
    {
      'target_name': 'libchromiumtypo',
      'type': 'shared_library',
      'dependencies': [
        '<(DEPTH)/third_party/WebKit/public/blink.gyp:blink',
        '<(DEPTH)/skia/skia.gyp:skia',
        '<(DEPTH)/url/url.gyp:url_lib',
        '<(DEPTH)/v8/tools/gyp/v8.gyp:v8',
      ],
      'ldflags': [
        '-Wl,--no-fatal-warnings',
      ],
    },
  ],
}
