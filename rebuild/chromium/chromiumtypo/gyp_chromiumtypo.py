#!/usr/bin/env python

# script will be run from temp mirror folder in chromium src (src/typoweb~)

import os
import sys

root = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))

sys.path.append(os.path.realpath(os.path.join(root, 'src/tools/gyp/pylib')))
sys.path.append(os.path.realpath(os.path.join(root, 'src/tools/grit')))
sys.path.append(os.path.realpath(os.path.join(root, 'src/build')))
sys.path.append(os.path.realpath(os.path.join(root, 'src/build/android/gyp')))

import gyp
import gyp_environment

if __name__ == "__main__":
    gyp_environment.SetEnvironment()
    args = sys.argv[1:]
    args.append(os.path.realpath(os.path.join(root, 'chromiumtypo/chromiumtypo.gyp')))
    args.append('-I' + os.path.realpath(os.path.join(root, 'src/build/common.gypi')))
    args.append('--no-circular-check')
    args.append('--check')
    args.append('--depth=' + os.path.realpath(os.path.join(root, 'src')))
    args.append('-Drelease_unwind_tables=0')
    args.append('-Dv8_use_external_startup_data=0')
    args.append('-Duse_openmax_dl_fft=0')
    args.append('-Denable_basic_printing=0')
    args.append('-Dfastbuild=1')

    print "Generating project build files..."

    sys.exit(gyp.main(args))
