solutions = [
  { "name"        : "src",
    "url"         : "https://chromium.googlesource.com/chromium/src.git@2994556db7593c516de30ce70f37ca3e51ad9ac3",
    "deps_file"   : ".DEPS.git",
    "managed"     : False,
    "custom_deps" : {
      "src/third_party/WebKit": "https://IgorDemin@bitbucket.org/IgorDemin/blink-typo.git@4b25f34cb6580373beb900fd46886e05604fe6c3",
    },
    "safesync_url": "",
  },
]
cache_dir = None
target_os = ['android']
