#!/bin/bash

set -e

. ./prepareEnv.sh

rm -rf $CHROMIUM_TARGET_FOLDER/include/

function copyInclude {
   mkdir --parents $CHROMIUM_TARGET_FOLDER/include/$1
   rsync -am --include='*.h' -f 'hide,! */' $CHROMIUMTYPO_ROOT/src/$1/ $CHROMIUM_TARGET_FOLDER/include/$1/
}

copyInclude base
copyInclude build
copyInclude skia/config
copyInclude ui/gfx
copyInclude url
copyInclude v8/include
copyInclude third_party/icu/source/common
copyInclude third_party/icu/source/i18n
copyInclude third_party/modp_b64
copyInclude third_party/skia/include
copyInclude third_party/skia/src
copyInclude third_party/WebKit/public
