#!/bin/bash

set -e

. ./prepareEnv.sh
export GYP_CROSSCOMPILE='1'
export GYP_DEFINES='OS=android target_arch=x64'
python $CHROMIUMTYPO_ROOT/chromiumtypo/gyp_chromiumtypo.py "$@"
chromiumTypo_ninjaReplaceSharedLibraryByStaticLibrary
chromiumTypo_setArch 'x86_64'

