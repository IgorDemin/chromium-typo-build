rm -f $1.mri
echo "create $1" >> $1.mri
for lib in ${@:2}
do
    echo "addlib $lib" >> $1.mri
done
echo "save" >> $1.mri
echo "end" >> $1.mri
ar -M <$1.mri
rm -f $1.mri

