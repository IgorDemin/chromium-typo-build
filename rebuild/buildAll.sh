#!/bin/bash

# todo починить билдинг mips_64 библиотеки

set -e

echo "Copying includes"
. ./copyIncludes.sh
echo "Copying assets"
. ./copyAssets.sh
echo "Building armv7_32"
. ./config_armv7_32.sh
. ./build.sh
echo "Building mips_32"
. ./config_mips_32.sh
. ./build.sh
echo "Building x86_32"
. ./config_x86_32.sh
. ./build.sh
#echo "Building armv8_64"
#. ./config_armv8_64.sh
#. ./build.sh
#echo "Building mips_64"
#. ./config_mips_64.sh
#. ./build.sh
#echo "Building x86_64"
#. ./config_x86_64.sh
#. ./build.sh
